# Space Invaders

## Building
Open the sln in visual studio to build and run

## License 
I chose the MIT license because it is a very basic open source license that doesn't add too many restrictions. Want to keep my repos as open as possible.

## Project Wiki
A CAPSTONE customer might want to choose a private repo because they might not want their code to be completely open. Especially if the code is for sensitive material that needs to be protected. Having the code under a private repo will still allow the team to use bitbucket as project management tool while keeping the needs for the customer.
