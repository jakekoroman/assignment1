﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace JKFinal
{
    public class Bullet
    {
        public static SoundEffect ShootSound { get; set; }
        public Vector2 Location { get; set; }
        public Rectangle BulletRectangle;
        public static Texture2D Texture { get; set; }
        private Vector2 Velocity;
        private const float SPEED = -7.5f;
        private const int SIZE = 32;

        /// <summary>
        /// Checks if bullet is still in the game window
        /// </summary>
        public bool IsActive
        {
            get
            {
                return Location.Y > 0;
            }
            set { }
        }

        /// <summary>
        /// Initializes the bullet
        /// </summary>
        /// <param name="xPos"> X position of the bullet </param>
        /// <param name="yPos"> Y position of the bullet </param>
        public Bullet(float xPos, float yPos)
        {
            this.Location = new Vector2(xPos, yPos);
            this.Velocity = new Vector2(0, SPEED);
            this.BulletRectangle = new Rectangle((int)xPos, (int)yPos, SIZE, SIZE);
            ShootSound.Play();
        }

        /// <summary>
        /// Creates a bullet based on the given position
        /// </summary>
        /// <param name="xPos"> X position of the bullet </param>
        /// <param name="yPos"> Y position of the bullet </param>
        /// <returns> New bullet based on the given position </returns>
        public static Bullet CreateBullet(float xPos, float yPos)
        {
            return new Bullet(xPos, yPos);
        }

        /// <summary>
        /// Moves the bullet up the screen
        /// </summary>
        public void Move()
        {
            Location = Vector2.Add(Location, Velocity);
            BulletRectangle.X = (int)Location.X;
            BulletRectangle.Y = (int)Location.Y;
        }
    }
}
