﻿using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using System;
using System.IO;
using System.Collections.Generic;

namespace JKFinal
{
    /// <summary>
    /// Static shared class that holds frequently needed data
    /// </summary>
    public static class Shared
    {
        public static int GameWidth { get; set; }
        public static int GameHeight { get; set; }
        public static KeyboardState OldState { get; set; }
        public static int HighestScore { get; set; }

        private static string highScoreFilePath = "HighScores.txt";

        /// <summary>
        /// Checks if two rectangles are colliding
        /// </summary>
        /// <param name="rect1"> first rectangle </param>
        /// <param name="rect2"> second rectangle</param>
        /// <returns> true if colliding, false if not </returns>
        public static bool IsColliding(Rectangle rect1, Rectangle rect2)
        {
            return rect1.Intersects(rect2);
        }

        /// <summary>
        /// Updates high score file with new higher score
        /// </summary>
        /// <param name="name"> Name of player </param>
        /// <param name="score"> Score the player got </param>
        public static void UpdateHighScoreFile(string name, int score)
        {
            List<string> result = new List<string>();
            List<string> names = new List<string>();
            List<int> scores = new List<int>();

            // Reads the current file
            using (StreamReader sr = new StreamReader(File.OpenRead(highScoreFilePath)))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] formattedLines = line.Split(',');
                    names.Add(formattedLines[0]);
                    scores.Add(Convert.ToInt32(formattedLines[1]));
                }
            }

            string previousName = string.Empty;
            int previousScore = -1;
            bool newScoreAdded = false;
            // loops the names
            for (int i = 0; i < names.Count; i++)
            {
                // checks if score is greater than or equal to the given score and updates the high score list accordingly
                if (score >= scores[i] && !newScoreAdded)
                {
                    result.Add($"{name},{score}");
                    newScoreAdded = true;
                    previousName = names[i];
                    previousScore = scores[i];
                    continue;
                }
                else if (newScoreAdded)
                {
                    result.Add($"{previousName},{previousScore}");
                    previousName = names[i];
                    previousScore = scores[i];
                }
                else
                {
                    result.Add($"{names[i]},{scores[i]}");
                }

                previousName = names[i];
            }

            // Writes the new high score list to the file
            File.Delete(highScoreFilePath);
            foreach (string line in result)
            {
                using (StreamWriter sw = new StreamWriter(highScoreFilePath, true))
                {
                    sw.WriteLine(line);
                }
            }
        }
    }
}


