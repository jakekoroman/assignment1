﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System.Collections.Generic;

namespace JKFinal
{
    public class AboutScreen : DrawableGameComponent
    {
        public SpriteBatch _spriteBatch { get; set; }
        public SpriteFont NormalFont { get; set; }
        public SpriteFont HighlightFont { get; set; }

        private readonly string goBack = "Back to Menu";
        private readonly string mainText = "Jake Koroman";

        private const int LINE_SPACE = 40;

        /// <summary>
        /// Initializes the about screen
        /// </summary>
        /// <param name="game"> current game </param>
        /// <param name="sb"> spritebatch for the game </param>
        /// <param name="normalFont"> font for unhighlighted text </param>
        /// <param name="highlightFont"> font for highlighted text </param>
        public AboutScreen(Game game, SpriteBatch sb, SpriteFont normalFont, SpriteFont highlightFont) : base(game)
        {
            this._spriteBatch = sb;
            this.NormalFont = normalFont;
            this.HighlightFont = highlightFont;
        }
        
        /// <summary>
        /// Override update method
        /// </summary>
        /// <param name="gameTime"> current game time </param>
        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();

            if ((ks.IsKeyDown(Keys.Space) && Shared.OldState.IsKeyUp(Keys.Space)) || (ks.IsKeyDown(Keys.Enter) && Shared.OldState.IsKeyUp(Keys.Enter)))
                Game1._currentState = Game1.GameState.Menu;

            Shared.OldState = ks;
            base.Update(gameTime);
        }

        /// <summary>
        /// Override draw method
        /// </summary>
        /// <param name="gameTime"> current game time </param>
        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            _spriteBatch.DrawString(NormalFont, mainText, new Vector2(Shared.GameWidth / 2 - LINE_SPACE * 2, LINE_SPACE), Color.White);

            _spriteBatch.DrawString(HighlightFont, goBack, new Vector2(10, Shared.GameHeight - LINE_SPACE), Color.Red);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
