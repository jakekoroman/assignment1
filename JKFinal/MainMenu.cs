﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace JKFinal
{
    public class MainMenu : DrawableGameComponent
    {
        public SpriteBatch _spriteBatch { get; set; }
        public SpriteFont NormalFont { get; set; }
        public SpriteFont HighlightFont { get; set; }

        private Color normalColour = Color.White;
        private Color highlightColour = Color.Red;

        private readonly List<string> MenuItems;
        private int selectedIndex = 0;
        private const int LINE_SPACE = 60;

        /// <summary>
        /// Initializes the main menu
        /// </summary>
        /// <param name="game"> current game </param>
        /// <param name="sb"> spritebatch for the game </param>
        /// <param name="menuItems"> all menu items to display </param>
        /// <param name="normalFont"> font for unhighlighted text </param>
        /// <param name="highlightFont"> font for highlighted text </param>
        public MainMenu(Game game, SpriteBatch sb, List<string> menuItems,
            SpriteFont normalFont, SpriteFont highlightFont) : base(game)
        {
            this._spriteBatch = sb;
            this.MenuItems = menuItems;
            this.NormalFont = normalFont;
            this.HighlightFont = highlightFont;
        }

        /// <summary>
        /// Overried update method
        /// </summary>
        /// <param name="gameTime"> current game time</param>
        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();

            if (ks.IsKeyDown(Keys.Up) && Shared.OldState.IsKeyUp(Keys.Up))
            {
                if (selectedIndex == 0)
                    selectedIndex = MenuItems.Count - 1;
                else
                    selectedIndex--;
            }
            else if (ks.IsKeyDown(Keys.Down) && Shared.OldState.IsKeyUp(Keys.Down))
            {
                if (selectedIndex == MenuItems.Count - 1)
                    selectedIndex = 0;
                else
                    selectedIndex++;
            }
            else if ((ks.IsKeyDown(Keys.Space) && Shared.OldState.IsKeyUp(Keys.Space)) || (ks.IsKeyDown(Keys.Enter) && Shared.OldState.IsKeyUp(Keys.Enter)))
            {
                switch (MenuItems[selectedIndex])
                {
                    case "Play":
                        Game1._currentState = Game1.GameState.Game;
                        break;
                    case "Help":
                        Game1._currentState = Game1.GameState.Help;
                        break;
                    case "About":
                        Game1._currentState = Game1.GameState.About;
                        break;
                    case "High Scores":
                        Game1._currentState = Game1.GameState.HighScores;
                        break;
                    case "Exit":
                        Game1._currentState = Game1.GameState.Exit;
                        break;
                }
            }

            Shared.OldState = ks;
            base.Update(gameTime);
        }

        /// <summary>
        /// Overried draw method
        /// </summary>
        /// <param name="gameTime"> current game time</param>
        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            for (int i = 0; i < MenuItems.Count; i++)
            {
                if (i == selectedIndex)
                    _spriteBatch.DrawString(HighlightFont, MenuItems[i], new Vector2(Shared.GameWidth / 2 - (NormalFont.Texture.Width / 4), Shared.GameHeight / 2 - (LINE_SPACE * MenuItems.Count / 2) + i * LINE_SPACE), highlightColour);
                else
                    _spriteBatch.DrawString(NormalFont, MenuItems[i], new Vector2(Shared.GameWidth / 2 - (NormalFont.Texture.Width / 4), Shared.GameHeight / 2 - (LINE_SPACE * MenuItems.Count / 2) + i * LINE_SPACE), normalColour);
            }

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
