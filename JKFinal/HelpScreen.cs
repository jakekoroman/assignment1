﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System.Collections.Generic;

namespace JKFinal
{
    public class HelpScreen : DrawableGameComponent
    {
        public SpriteBatch _spriteBatch { get; set; }
        public SpriteFont NormalFont { get; set; }
        public SpriteFont HighlightFont { get; set; }

        private readonly string filePath = "HelpScreen.txt";
        private readonly string goBack = "Back to Menu";
        private readonly List<string> HelpScreenText = new List<string>();
        private const int LINE_SPACE = 40;

        /// <summary>
        /// Initializes the help screen
        /// </summary>
        /// <param name="game"> current game </param>
        /// <param name="sb"> spritebatch for the game </param>
        /// <param name="normalFont"> font for unhighlighted text </param>
        /// <param name="highlightFont"> font for highlighted text </param>
        public HelpScreen(Game game, SpriteBatch sb, SpriteFont normalFont, SpriteFont highlightFont) : base(game)
        {
            this._spriteBatch = sb;
            this.NormalFont = normalFont;
            this.HighlightFont = highlightFont;
            HelpScreenText = ReadFile();
        }

        /// <summary>
        /// Reads the help screen text file and puts it into a formatted list to display on the screen
        /// </summary>
        /// <returns> a list with all the file lines </returns>
        private List<string> ReadFile()
        {
            List<string> result = new List<string>();

            using (StreamReader sr = new StreamReader(File.OpenRead(filePath)))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    result.Add(line);
                }
            }

            return result;
        }

        /// <summary>
        /// Override update method
        /// </summary>
        /// <param name="gameTime"> current game time </param>
        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();

            if ((ks.IsKeyDown(Keys.Space) && Shared.OldState.IsKeyUp(Keys.Space)) || (ks.IsKeyDown(Keys.Enter) && Shared.OldState.IsKeyUp(Keys.Enter)))
                Game1._currentState = Game1.GameState.Menu;

            Shared.OldState = ks;
            base.Update(gameTime);
        }

        /// <summary>
        /// Override draw method
        /// </summary>
        /// <param name="gameTime"> current game time </param>
        public override void Draw(GameTime gameTime)
        {
            const int START_LOC = 10;

            _spriteBatch.Begin();

            for (int i = 0; i < HelpScreenText.Count; i++)
                _spriteBatch.DrawString(NormalFont, HelpScreenText[i], new Vector2(START_LOC, START_LOC + (i * LINE_SPACE)), Color.White);
            
            _spriteBatch.DrawString(HighlightFont, goBack, new Vector2(START_LOC, Shared.GameHeight - LINE_SPACE), Color.Red);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
