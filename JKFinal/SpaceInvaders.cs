﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace JKFinal
{
    public class SpaceInvaders : DrawableGameComponent
    {
        public SpriteBatch _spriteBatch { get; set; }
        public SpriteFont _spriteFont { get; set; }
        public ContentManager _contentManager { get; set; }

        public static int Score { get; set; }

        private const int PLAYER_SIZE = 64;

        private readonly Ship Player;
        private Rectangle playerRectangle;
        private Vector2 playerSize;

        private readonly List<Bullet> bullets;
        private readonly List<Enemy> enemies;
        private KeyboardState _keyboardState;

        /// <summary>
        /// SpaceInvaders Constructor
        /// </summary>
        /// <param name="game"> Game to control </param>
        /// <param name="sb"> Spritebatch used by the game </param>
        /// <param name="sf"> Default spritefont to be used to draw score text </param>
        /// <param name="cm"> Content manager used by the game /param>
        public SpaceInvaders(Game game, SpriteBatch sb, SpriteFont sf, ContentManager cm) : base(game)
        {
            this._spriteBatch = sb;
            this._spriteFont = sf;
            this._contentManager = cm;

            this.Player = new Ship(Shared.GameWidth / 2 - (PLAYER_SIZE / 2), Shared.GameHeight - PLAYER_SIZE, PLAYER_SIZE);
            this.playerSize = new Vector2(PLAYER_SIZE, PLAYER_SIZE);
            this.playerRectangle = new Rectangle((int)Player.Location.X, (int)Player.Location.Y, (int)this.playerSize.X, (int)this.playerSize.Y);

            this.bullets = new List<Bullet>();
            this.enemies = new List<Enemy>();
            AddEnemies();
        }

        /// <summary>
        /// Loads textures through the content manager
        /// </summary>
        public void LoadTextures()
        {
            Player.Texture = _contentManager.Load<Texture2D>("images/ship");
            Bullet.Texture = _contentManager.Load<Texture2D>("images/bullet");
            Enemy.Texture = _contentManager.Load<Texture2D>("images/alien");

            Bullet.ShootSound = _contentManager.Load<SoundEffect>("audio/shoot");
        }

        /// <summary>
        /// Override update method
        /// </summary>
        /// <param name="gameTime"> Current gametime </param>
        public override void Update(GameTime gameTime)
        {
            MovePlayer();
            GameplayHandler();
            EndGame();
            base.Update(gameTime);
        }

        /// <summary>
        /// Override draw method
        /// </summary>
        /// <param name="gameTime"> Current gametime </param>
        public override void Draw(GameTime gameTime)
        {
            Vector2 textLocation = new Vector2(PLAYER_SIZE / 4, Shared.GameHeight - PLAYER_SIZE * 2);

            _spriteBatch.Begin();

            _spriteBatch.Draw(Player.Texture, playerRectangle, Color.White);

            foreach (Bullet bullet in bullets)
                _spriteBatch.Draw(Bullet.Texture, bullet.BulletRectangle, Color.White);

            foreach (Enemy enemy in enemies)
                _spriteBatch.Draw(Enemy.Texture, enemy.EnemyRectangle, Color.White);

            _spriteBatch.DrawString(_spriteFont, $"Score: {Score}\nHighest Score: {Shared.HighestScore}", textLocation, Color.White);

            _spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Ends the game based on what happened. If player kills all enemies or enemies reach the end
        /// </summary>
        private void EndGame()
        {
            if (enemies.Count == 0 || Enemy.ReachedBottom)
            {
                Shared.UpdateHighScoreFile(Environment.UserName, Score);
                Game1._currentState = Game1.GameState.EndGame;
            }
        }

        /// <summary>
        /// Handles moving the player based on what key is pressed
        /// </summary>
        private void MovePlayer()
        {
            _keyboardState = Keyboard.GetState();

            if ((_keyboardState.IsKeyDown(Keys.Left) || _keyboardState.IsKeyDown(Keys.A)) && !Player.IsTooLeft)
                Player.ChangeDirection(Keys.Left);
            else if ((_keyboardState.IsKeyDown(Keys.Right) || _keyboardState.IsKeyDown(Keys.D)) && !Player.IsTooRight)
                Player.ChangeDirection(Keys.Right);

            playerRectangle.X = (int)Player.Location.X;
            playerRectangle.Y = (int)Player.Location.Y;
        }

        /// <summary>
        /// Handles the main gameplay logic
        /// Bullets, Enemies, Collision
        /// </summary>
        private void GameplayHandler()
        {
            AddBullet();

            // handles hitting the bullet moving and collision
            // sometimes throws index out of range due to how fast bullets are being created and deleted
            try
            {
                for (int i = 0; i < bullets.Count; i++)
                {
                    if (bullets[i].IsActive)
                    {
                        bullets[i].Move();
                        if (enemies.Count > 0)
                        {
                            for (int j = 0; j < enemies.Count; j++)
                            {
                                if (Shared.IsColliding(bullets[i].BulletRectangle, enemies[j].EnemyRectangle))
                                {
                                    bullets.RemoveAt(i);
                                    enemies.RemoveAt(j);
                                    Score += 100;
                                }
                            }
                        }
                    }
                    else
                    {
                        bullets.RemoveAt(i);
                    }
                }
            }
            catch (System.Exception)
            {
            }

            // Moves enemies
            foreach (Enemy enemy in enemies)
                enemy.Move();
        }

        /// <summary>
        /// Adds enemies to the enemies list
        /// only called in the constructor and reset method
        /// </summary>
        private void AddEnemies()
        {
            int numOfEnemies = Shared.GameWidth / Enemy.Spacer / 2;

            for (int i = 0; i < numOfEnemies; i++)
                for (int j = 0; j < numOfEnemies; j++)
                    enemies.Add(new Enemy(i * Enemy.Spacer, j * Enemy.Spacer));
        }

        /// <summary>
        /// Handles adding bullets to the bullets list
        /// </summary>
        private void AddBullet()
        {
            _keyboardState = Keyboard.GetState();

            if (_keyboardState.IsKeyDown(Keys.Space) && Shared.OldState.IsKeyUp(Keys.Space))
                bullets.Add(Bullet.CreateBullet(Player.Location.X + (PLAYER_SIZE / 3), Player.Location.Y));

            Shared.OldState = _keyboardState;
        }

        /// <summary>
        /// Resets the game back to the start
        /// </summary>
        public void Reset()
        {
            Score = 0;
            Player.Location = Player.DefaultLocation;
            bullets.Clear();
            enemies.Clear();
            AddEnemies();
        }
    }
}
