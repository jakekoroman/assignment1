﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System.Collections.Generic;
using System;

namespace JKFinal
{
    public class HighScoreScreen : DrawableGameComponent
    {
        public SpriteBatch _spriteBatch { get; set; }
        public SpriteFont NormalFont { get; set; }
        public SpriteFont HighlightFont { get; set; }

        private readonly string filePath = "HighScores.txt";
        private readonly string goBack = "Back to Menu";
        private List<string> HighScores = new List<string>();
        private const int LINE_SPACE = 40;

        /// <summary>
        /// Initializes the high score screen
        /// </summary>
        /// <param name="game"> current game </param>
        /// <param name="sb"> spritebatch for the game </param>
        /// <param name="normalFont"> normal font for unhighlighted text </param>
        /// <param name="highlightFont"> font for highlighted text </param>
        public HighScoreScreen(Game game, SpriteBatch sb, SpriteFont normalFont, SpriteFont highlightFont) : base(game)
        {
            this._spriteBatch = sb;
            this.NormalFont = normalFont;
            this.HighlightFont = highlightFont;
            HighScores = ReadFile();
        }

        /// <summary>
        /// Reads The current high score file
        /// </summary>
        /// <returns> returns list of the highscores to display, formatted nicely </returns>
        private List<string> ReadFile()
        {
            List<string> result = new List<string>();

            using (StreamReader sr = new StreamReader(File.OpenRead(filePath)))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] formattedLines = line.Split(',');
                    result.Add($"{formattedLines[1]} - {formattedLines[0]}");
                }
            }

            // sets highest score
            Shared.HighestScore = Convert.ToInt32(result[0].Substring(0, result[0].IndexOf('-')));

            return result;
        }

        /// <summary>
        /// Override update method
        /// </summary>
        /// <param name="gameTime"> current gametime </param>
        public override void Update(GameTime gameTime)
        {
            HighScores = ReadFile();
            KeyboardState ks = Keyboard.GetState();

            if ((ks.IsKeyDown(Keys.Space) && Shared.OldState.IsKeyUp(Keys.Space)) || (ks.IsKeyDown(Keys.Enter) && Shared.OldState.IsKeyUp(Keys.Enter)))
                Game1._currentState = Game1.GameState.Menu;

            Shared.OldState = ks;
            base.Update(gameTime);
        }

        /// <summary>
        /// Override Draw method
        /// </summary>
        /// <param name="gameTime"> current gametime </param>
        public override void Draw(GameTime gameTime)
        {
            const int START_LOC = 10;

            _spriteBatch.Begin();

            for (int i = 0; i < HighScores.Count; i++)
                _spriteBatch.DrawString(NormalFont, HighScores[i], new Vector2(START_LOC, START_LOC + (i * LINE_SPACE)), Color.White);

            _spriteBatch.DrawString(HighlightFont, goBack, new Vector2(START_LOC, Shared.GameHeight - LINE_SPACE), Color.Red);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
