﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace JKFinal
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        public enum GameState
        {
            Menu,
            Help,
            About,
            HighScores,
            Game,
            EndGame,
            Exit
        }

        public static GameState _currentState;

        private readonly List<string> MenuOptions = new List<string>()
        {
            "Play",
            "Help",
            "About",
            "High Scores",
            "Exit"
        };

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private SpriteFont NormalFont, HighlightFont;
        private MainMenu Menu;
        private HelpScreen HelpScreen;
        private AboutScreen AboutScreen;
        private HighScoreScreen HighScoreScreen;
        private SpaceInvaders GameScreen;
        private EndGameScreen EndGameScreen;

        private SoundEffect backgroundMusic;
        private SoundEffectInstance _songInstance;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            // sets window dimensions
            graphics.PreferredBackBufferWidth = 512;
            graphics.PreferredBackBufferHeight = 512;
            graphics.ApplyChanges();

            // updates the shared info on the window
            Shared.GameWidth = graphics.PreferredBackBufferWidth;
            Shared.GameHeight = graphics.PreferredBackBufferHeight;

            _currentState = GameState.Menu;


            SoundEffect.MasterVolume = 0.025f;


            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            NormalFont = Content.Load<SpriteFont>("Fonts/NormalFont");
            HighlightFont = Content.Load<SpriteFont>("Fonts/HighlightFont");
            backgroundMusic = Content.Load<SoundEffect>("audio/background");

            _songInstance = backgroundMusic.CreateInstance();

            Menu = new MainMenu(this, spriteBatch, MenuOptions, NormalFont, HighlightFont);
            HelpScreen = new HelpScreen(this, spriteBatch, NormalFont, HighlightFont);
            AboutScreen = new AboutScreen(this, spriteBatch, NormalFont, HighlightFont);
            HighScoreScreen = new HighScoreScreen(this, spriteBatch, NormalFont, HighlightFont);
            EndGameScreen = new EndGameScreen(this, spriteBatch, NormalFont, HighlightFont);
            GameScreen = new SpaceInvaders(this, spriteBatch, NormalFont, Content);

            GameScreen.LoadTextures();
            _songInstance.IsLooped = true;
            _songInstance.Play();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: add reset when returning back to menu
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                _currentState = GameState.Menu;
                GameScreen.Reset();
            }

            // TODO: Add your update logic here
            switch (_currentState)
            {
                case GameState.Menu:
                    Menu.Update(gameTime);
                    GameScreen.Reset();
                    break;
                case GameState.Help:
                    HelpScreen.Update(gameTime);
                    break;
                case GameState.About:
                    AboutScreen.Update(gameTime);
                    break;
                case GameState.HighScores:
                    HighScoreScreen.Update(gameTime);
                    break;
                case GameState.Game:
                    GameScreen.Update(gameTime);
                    break;
                case GameState.EndGame:
                    EndGameScreen.Update(gameTime);
                    break;
                case GameState.Exit:
                    Exit();
                    break;
                default:
                    break;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            switch (_currentState)
            {
                case GameState.Menu:
                    Menu.Draw(gameTime);
                    break;
                case GameState.Help:
                    HelpScreen.Draw(gameTime);
                    break;
                case GameState.About:
                    AboutScreen.Draw(gameTime);
                    break;
                case GameState.HighScores:
                    HighScoreScreen.Draw(gameTime);
                    break;
                case GameState.EndGame:
                    EndGameScreen.Draw(gameTime);
                    break;
                case GameState.Game:
                    GameScreen.Draw(gameTime);
                    break;
                default:
                    break;
            }

            base.Draw(gameTime);
        }
    }
}
