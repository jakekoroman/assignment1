﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace JKFinal
{
    public class Enemy
    {
        public Vector2 Location;
        public Rectangle EnemyRectangle;
        public static Texture2D Texture { get; set; }
        public static bool ReachedBottom { get; set; }
        public static int Spacer { get => SPACER; }
        private Vector2 Velocity;
        private const float SPEED = 5f;
        private const int SIZE = 32;
        private const int SPACER = 40;

        /// <summary>
        /// Enemy constructor
        /// </summary>
        /// <param name="xPos"> x position </param>
        /// <param name="yPos"> y position </param>
        public Enemy(float xPos, float yPos)
        {
            Location = new Vector2(xPos, yPos);
            Velocity = new Vector2(SPEED, 0);
            EnemyRectangle = new Rectangle((int)xPos, (int)yPos, SIZE, SIZE);
            ReachedBottom = false;
        }

        /// <summary>
        /// Returns a new enemy based on the given coordinates
        /// </summary>
        /// <param name="xPos"> x position </param>
        /// <param name="yPos"> y position </param>
        /// <returns></returns>
        public static Enemy CreateEnemy(float xPos, float yPos)
        {
            return new Enemy(xPos, yPos);
        }

        /// <summary>
        /// Moves the enemies
        /// </summary>
        public void Move()
        {
            Location = Vector2.Add(Location, Velocity);
            EnemyRectangle.X = (int)Location.X;
            EnemyRectangle.Y = (int)Location.Y;

            if (Location.X >= Shared.GameWidth - SIZE || Location.X <= 0)
            {
                Location.Y += SPACER / 2;
                EnemyRectangle.Y = (int)Location.Y;
                Velocity.X *= -1;
            }

            if (Location.Y > Shared.GameHeight - SIZE * 2)
                ReachedBottom = true;
        }
    }
}