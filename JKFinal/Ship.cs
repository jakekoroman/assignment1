﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JKFinal
{
    public class Ship
    {
        public Vector2 Location { get; set; }
        public Texture2D Texture { get; set; }
        public Vector2 DefaultLocation { get; set; }
        public bool CanShoot { get => true; set { } }

        public bool IsTooLeft
        {
            get
            {
                return Location.X <= 0;
            }
        }

        public bool IsTooRight
        {
            get
            {
                return Location.X >= Shared.GameWidth - size;
            }
        }

        private Vector2 Velocity;
        private const float SPEED = 5f;
        private int size;

        public Ship(float xPos, float yPos, int size)
        {
            Location = new Vector2(xPos, yPos);
            Velocity = new Vector2(SPEED, 0);
            DefaultLocation = Location;
            this.size = size;
        }

        private void Move()
        {
            Location = Vector2.Add(Location, Velocity);
        }

        public void ChangeDirection(Keys key)
        {
            if (key.Equals(Keys.Left))
                Velocity.X = -SPEED;
            else if (key.Equals(Keys.Right))
                Velocity.X = SPEED;

            Move();
        }
    }
}
